# daswin-client

> Nuxt.js project for Daswin office tower

## Build Setup

``` bash
# install dependencies
$ yarn install

# serve with hot reload at localhost:3000
$ yarn run dev

# build for production and launch server
$ yarn run build
$ yarn start

# generate static project
$ yarn run generate
```

## List Page
1. Homepage
2. Our Building
3. Our Location
4. Sustainability
5. Building Spesification
6. Project Update
7. Inquiries