// $(".jellybo-MenuIcon004").click(function () {
//     if ($(this).jellyboMenuIcon004().isOpened()) {
//         $(this).jellyboMenuIcon004().close();
//     } else {
//         $(this).jellyboMenuIcon004().open();
//     }
// });

// $(window).scroll(function () {
//     if ($(this).scrollTop() > 1) {
//         $('#daswin-navbar').addClass('daswin-navbar--fixed');
//     } else {
//         $('#daswin-navbar').removeClass('daswin-navbar--fixed');
//     }
// });

$("a[href^='#']").click(function(e) {
  console.log(e)
	e.preventDefault();
	
	var position = $($(this).attr("href")).offset().top;

	$("body, html").animate({
		scrollTop: position
	} /* speed */ );
});

// $('a[href^="#"]').on('click', function(event) {
//   var target = $( $(this).attr('href') );
//   if( target.length ) {
//       event.preventDefault();
//       $('html, body').animate({
//           scrollTop: target.offset().top
//       }, 500);
//   }
// });

$(document).ready(function(){
	$("#btn1").click(function(){
    $("#box").animate({height: "300px"});
  });
  $("#btn2").click(function(){
    $("#box").animate({height: "100px"});
  });

  $( ".susta" ).click(function() {
    var a = $( "section.sustainability-scroll" ).offset().top
    console.log(a)
		$("html, body").animate({
			scrollTop: $( "section.sustainability-scroll" ).offset().top
    }, 1000);
		return false;
  });
  
  $( "#go" ).click(function() {
    $( "#block" ).animate({
      width: "70%",
      opacity: 0.4,
      marginLeft: "0.6in",
      fontSize: "3em",
      borderWidth: "10px"
    }, 1500 );
  });
});

/*-----
Spanizer
- Wraps letters with spans, for css animations
-----*/
(function($) {
  var s,
  spanizeLetters = {
    settings: {
      letters: $('.js-spanize'),
    },
    init: function() {
      s = this.settings;
      this.bindEvents();
    },
    bindEvents: function(){
      s.letters.html(function (i, el) {
        //spanizeLetters.joinChars();
        var spanizer = $.trim(el).split("");
        return '<span>' + spanizer.join('</span><span>') + '</span>';
      });
    },
  };
  spanizeLetters.init();
})(jQuery);